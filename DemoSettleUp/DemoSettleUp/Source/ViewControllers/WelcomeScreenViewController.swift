//
//  WelcomeScreenViewController.swift
//  DemoSettleUp
//
//  Created by Stanislav Kasprik on 01/10/2017.
//  Copyright © 2017 @StanislavK. All rights reserved.
//

import UIKit
import SnapKit

class WelcomeScreenViewController: UIViewController {

    // MARK: - Private Properties

    private lazy var collectionView: UICollectionView = {
        // CollectionView layout
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.bounces = false

        return collectionView
    }()

    private let cellId = "WelcomePageCell"

    private let pages: [WelcomePage] = {
        return [
            WelcomePage(title: "Sharing expenses with friends?", message: "Settle Up keeps track of your gang's expenses - great for travellers, flatmates, couples and others.", imageName: "Page1"),
            WelcomePage(title: "Free your wallet of receipts", message: "All expenses are backed up\nand synced across the group\nso each member can see them.", imageName: "Page2"),
            WelcomePage(title: "Good accounting makes good friends", message: "No more disputes over the bill. Settle Up shows who pays next and minimizes the transactions.", imageName: "Page3"),
            ]
    }()

    private lazy var containerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .clear
        return containerView
    }()

    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1)
        pageControl.numberOfPages = 3

        return pageControl
    }()

    private lazy var skipButton: UIButton = {
        let skipButton = UIButton(type: .system)
        skipButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        skipButton.setTitle("SKIP", for: .normal)
        skipButton.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        skipButton.backgroundColor = .clear
        skipButton.addTarget(self, action: #selector(skipButtonTapped(sender:)), for: .touchUpInside)

        return skipButton
    }()

    private lazy var nextButton: UIButton = {
        let nextButton = UIButton(type: .system)
        nextButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        nextButton.setTitle(">", for: .normal)
        nextButton.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        nextButton.backgroundColor = .clear
        nextButton.addTarget(self, action: #selector(nextButtonTapped(sender:)), for: .touchUpInside)

        return nextButton
    }()

    private lazy var containerStackView: UIStackView = {
        let containerStackView = UIStackView()
        containerStackView.axis = .horizontal
        containerStackView.alignment = .fill
        containerStackView.distribution = .fillEqually

        return containerStackView
    }()

    // MARK: - View Life-cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(WelcomePageCell.self, forCellWithReuseIdentifier: cellId)

        // Add subviews
        view.addSubview(collectionView)
        view.addSubview(containerView)

        view.addSubview(containerStackView)
        containerStackView.addArrangedSubview(skipButton)
        containerStackView.addArrangedSubview(pageControl)
        containerStackView.addArrangedSubview(nextButton)

        // Autolayout
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.snp.edges)
        }

        containerView.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.left)
            make.right.equalTo(view.snp.right)
            make.bottom.equalTo(view.snp.bottom)
            make.height.equalTo(44)
        }

        containerStackView.snp.makeConstraints { (make) in
            make.edges.equalTo(containerView.snp.edges)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> delegate

extension WelcomeScreenViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! WelcomePageCell

        let page = pages[(indexPath as NSIndexPath).item]
        cell.page = page

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }

    // ScrollView
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = Int(targetContentOffset.pointee.x / view.frame.width)
        pageControl.currentPage = pageNumber
    }
}

// MARK: - Private Methods in extension

private extension WelcomeScreenViewController {

    @objc func skipButtonTapped(sender: UIButton) {
        // TODO: -- to be implemented
        print(#function)
    }

    @objc func nextButtonTapped(sender: UIButton) {
        // TODO: -- to be implemented
        print(#function)
    }
}
