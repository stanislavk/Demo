//
//  WelcomePageCell.swift
//  DemoSettleUp
//
//  Created by Stanislav Kasprik on 01/10/2017.
//  Copyright © 2017 @StanislavK. All rights reserved.
//

import UIKit
import SnapKit

class WelcomePageCell: UICollectionViewCell {

    // MARK: - Public Properties

    var page: WelcomePage? {
        didSet {
            guard let page = page else { return }
            // Setup image
            imageView.image = UIImage(named: page.imageName)
            // Setup attributed text
            setupAttributedTextForTextView(page)
        }
    }

    // MARK: - Private Properties

    private lazy var imageContainerView: UIView = {
        let imageContainerView = UIView()
        imageContainerView.backgroundColor = UIColor(named: AppColors.welcomeScreenBackgroundColor.value)

        return imageContainerView
    }()

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear

        return imageView
    }()

    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isSelectable = false
        textView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        textView.backgroundColor = .clear

        return textView
    }()

    private lazy var lineSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.9, alpha: 1)

        return view
    }()

    // MARK: - View Life-cycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - -- private methods

private extension WelcomePageCell {

    func setupViews() {
        // Add subviews
        addSubview(imageContainerView)
        addSubview(imageView)
        addSubview(lineSeparatorView)
        addSubview(textView)

        // Add layout
        imageContainerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.65)
        }
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(250)
            make.height.equalTo(250)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(64)
        }
        textView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.35)
        }
        lineSeparatorView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(textView.snp.top)
        }
    }

    func setupAttributedTextForTextView(_ page: WelcomePage) {
        let color = UIColor(white: 0.2, alpha: 1)
        let attributedText = NSMutableAttributedString(string: page.title, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold), NSAttributedStringKey.foregroundColor: color])
        attributedText.append(NSAttributedString(string: "\n\n\(page.message)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: color]))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let length = attributedText.string.characters.count
        attributedText.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: length))

        textView.attributedText = attributedText
    }
}
