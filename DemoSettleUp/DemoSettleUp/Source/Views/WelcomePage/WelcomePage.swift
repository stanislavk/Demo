//
//  WelcomePage.swift
//  DemoSettleUp
//
//  Created by Stanislav Kasprik on 01/10/2017.
//  Copyright © 2017 @StanislavK. All rights reserved.
//

/// Model for page used in the WelcomeScreenViewController
struct WelcomePage {
    let title: String
    let message: String
    let imageName: String
}
