//
//  Configuration.swift
//  DemoSettleUp
//
//  Created by Stanislav Kasprik on 01/10/2017.
//  Copyright © 2017 @StanislavK. All rights reserved.
//

enum AppColors {
    case welcomeScreenBackgroundColor

    var value: String {
        switch self {
        case .welcomeScreenBackgroundColor: return "WelcomeScreenBackgroundColor"
        }
    }
}
