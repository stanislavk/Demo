//
//  AppDelegate.swift
//  DemoSettleUp
//
//  Created by Stanislav Kasprik on 01/10/2017.
//  Copyright © 2017 @StanislavK. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

